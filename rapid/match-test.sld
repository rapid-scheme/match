;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid match-test)
  (export run-tests)
  (import (scheme base)
          (rapid test)
	  (rapid match))
  (begin
    (define (run-tests)
      (test-begin "A pattern matcher")

      (test-group "Match literals"

	(test-equal "Match number"
	  2
	  (match 2
	    (1 #f)
	    (2 2)))

	(test-equal "Match symbol"
	  #t
	  (match 'x
	    (x #t)
	    (y #f))))

      (test-group "List matches"
	
	(test-equal "Match list"
	  1
	  (match '(1 2)
	    ((,x) #f)
	    ((,x ,y ,z) #f)
	    ((,x ,y) x)))

	(test-equal "Match empty list"
	  'empty
	  (match '()
	    ((,x) #f)
	    (() 'empty)))

	(test-equal "Match dotted list"
	  '(2 3)
	  (match '(1 2 3)
	    ((1 . 3) #f)
	    ((1 . ,w) w)))

	(test-equal "Match dotted list with empty tail"
	  '((100) ())
	  (match '(100)
	    ((,x ... . ,y) (list x y))))

	(test-equal "Match empty list with tail"
	  '(() 200)
	  (match '200
		 ((,x ... . ,y) (list x y))))

	(test-equal "Match empty list with empty tail"
	  '(() ())
	  (match '()
	    ((,x ... . ,y) (list x y)))))

      (test-group "Catamorphisms"

	(test-equal "Simple recursion"
	  'foo
	  (match '(begin (bar foo))
	    ((bar ,x) x)
	    ((begin ,(x)) x)))

	(test-equal "Splitting values"
	  '((a c e) (b d f))
	  (let-values
	      ((result 
		(match '(a b c d e f)
		  (() (values '() '()))
		  ((,x) (values `(,x) '()))
		  ((,x ,y . ,(odds evens))
		   (values (cons x odds)
			   (cons y evens))))))
	    result))

	(test-equal "More catamorphisms"
	  '(2 3 4)
	  (let ((add1 (lambda (x)
			(+ x 1))))
	    (match '(if 1 2 3)
	      ((if ,(add1 -> x)
		   ,(add1 -> y)
		   ,(add1 -> z))
	       (list x y z))
	      (,_ #f)))))

      (test-group "Guard expressions"

	(test-equal "Static guard"
	  'x
	  (match 'x
	    (x (guard #f) #f)
	    (x (guard #t) 'x))))

      (test-group "Pattern variables"

	(test-equal "Simple pattern variable"
	  'foo
	  (match 'foo	 
	    (x #f)
	    (,x x))))

      (test-group "Ellipsis"

	(test-equal "Simple ellipsis case"
	  '(foo bar)
	  (match '(qux foo bar baz)
	    ((qux ,x ... bla) #f)
	    ((qux ,x ... baz) x)))

	(test-equal "Two variables before ellipsis"
	  '((foo bar) (1 2))
	  (match '(qux (foo 1) (bar 2) baz)
	    ((qux (,x ,y) ... baz) `(,x ,y))))

	(test-equal "More than one ellipsis"
	  '((foo 1) (bar 2))
	  (match '(qux (foo 1) (bar 2) baz)
	    ((qux (,x ...) ... baz) x)))

	(test-equal "Empty list"
	  '()
	  (match '(foo)
	    ((foo (,x* ,y*) ...)
	     (map list x* y*))
	    (,_ #f)))

	(test-equal "Catamorphisms and ellipses"
	  '(1 2 3)
	  (match '(a b c)
	    ((,(a) ... ,(c))
	     `(,@a ,c))
	    (a 1)
	    (b 2)
	    (c 3))))

      (test-group "Vector matches"
	(test-equal "Match vector"
	  #(a b)
	  (match #(a b)
	    (#(a b c)
	     #f)
	    (#(a c)
	     #f)
	    (#(a ,b)
	     (vector 'a b))))

	(test-equal "Match vector with ellipsis"
	  #(1 2 3)
	  (match #((a 1) (a 2) (a 3))
	    (#((a ,x) ... (a ,y))
	     `#(,@x ,y))))

	(test-equal "Vectors and catamorphisms"
	  #(1 2 3)
	  (match #(a b c)
	    (#(,(a) ,(b) ,(c))
	     `#(,a ,b ,c))
	    (a 1)
	    (b 2)
	    (c 3)))

	(test-equal "Vectors, catamorphisms, and ellipses"
	  #(0 1 2 3)
	  (match #(x a b c)
	    (#(,(x) ,(a) ... ,(c))
	     `#(,x ,@a ,c))
	    (x 0)
	    (a 1)
	    (b 2)
	    (c 3))))
      
      (test-group "Automatic unpacking"
	(define-record-type <box>
	  (box value)
	  box?
	  (value %unbox))
	(define (unbox b)
	  (if (box? b)
	      (let ((value (%unbox b)))
		(cond
		 ((pair? value)
		  (cons (car value) (box (cdr value))))
		 (else
		  value)))
	      b))

	(test-equal "Boxed value"
	  '(2 #t)
	  (match* (unbox #f) (box '(1 2 3))
	    ((1 ,b . ,c)
	     (list b (box? c)))))

	(test-assert "Boxed value as tail"
		     (match* (unbox #f) `(,(box 1) ,(box 2) . ,(box 3))
			     ((,a ... . ,b) (box? b))))

	(test-equal "Boxed empty list"
	  '(() ())
	  (match* (unbox #f) (box '())
		  ((,x ... . ,y) (list x y))))

	(test-equal "Boxing"
	  '(#t #t)
	  (match* (unbox box) (box '(1 2 3))
	    ((1 ,(b) . ,c)
	     (list (box? b) (box? c))))))

      (test-group "Circular lists"
	(test-equal "Simple circular lists"
	  '(1 2 3 1)
	  (match '#0=(1 2 3 . #0#)
		 ((,a ,b ,c . ,d) (list a b c (car d)))
		 (,x 'circular)))

	(test-equal "Ellipses and circular lists"
	  'circular
	  (match '#0=(1 2 3 . #0#)
		 ((,a ... . ,b) (cons a b))
		 (,x 'circular))))
      
      (test-end))))
